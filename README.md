# Sorting App Project

1. Maven Project of the model version 4.0.0 should be used.

2. Java 8 as a source and target version for the Compile stage should be used.

3. Maven properties in the configuration should be used.

4. A dependency should be introduced, for example, the Commons-IO library.

5. Packages should be used.

6. Javadoc for a public API should be created.

7. Unit tests should be created with the help of JUnit 4.12.

8. Parametrized unit tests should be used.

9. Tests for the application should be created and corner cases should be covered. Corner cases are the cases with zero, one, 
ten, and more than 10 arguments. To know more about corner cases, please review an article.

10. A project should be configured to build a runnable jar.

11. Resource files should be used in the project (optional).
