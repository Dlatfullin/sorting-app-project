package com.dlatfullin;

import java.util.Arrays;

public class SortingApp {

    /**
     * The main method that starts the application.
     *
     * @param args the command line
     */
    public static void main(String[] args) {
        if (args.length > 10) {
            System.out.println("I takes up to 10 integer arguments");
            return;
        }

        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
        } catch (NumberFormatException e) {
            System.out.println("I only takes up integers");
            return;
        }

        Arrays.sort(numbers);

        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
