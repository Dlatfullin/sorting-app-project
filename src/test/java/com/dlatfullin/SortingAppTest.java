package com.dlatfullin;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private String[] input;
    private String expected;


    public SortingAppTest(String[] input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void tearDown() {
        System.setOut(originalOut);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{}, ""},
                {new String[]{"5"}, "5"},
                {new String[]{"5", "3", "8", "1", "9"}, "1 3 5 8 9"},
                {new String[]{"5", "3", "8", "1", "9", "2", "7", "6", "4", "0"}, "0 1 2 3 4 5 6 7 8 9"}
        });
    }

    @Test
    public void testSorting() {
        SortingApp.main(input);
        Assert.assertEquals(expected, outContent.toString().trim());
    }

    @Test
    public void testNoIntegerInput() {
        String[] input = {"5", "3", "a", "1", "9"};
        SortingApp.main(input);
        Assert.assertEquals("I only takes up integers", outContent.toString().trim());
    }

    @Test
    public void testMoreThanTenInput() {
        String[] input = {"5", "3", "7", "1", "9", "2", "6", "5", "11", "22", "11"};
        SortingApp.main(input);
        Assert.assertEquals("I takes up to 10 integer arguments", outContent.toString().trim());
    }

}
